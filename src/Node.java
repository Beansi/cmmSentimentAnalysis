import java.util.*;
import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.misc.Graph;
import org.antlr.v4.runtime.*;

enum NodeType{ terminal, prog, stmt, declStmt, deAs, ifStmt, whileStmt, assignStmt ,
    writeStmt, stmtBlock, forStmt, type, constant, compare, variable, relation, expr, expr1, expr2, expr3, expr4}

enum visitFlag{ visited, unvisited, finished}

public class Node {
    public static final int
            FOR=1, IF=2, ELSE=3, ELSE_IF=4, WHILE=5, BREAK=6, Continue=7, T_DOUBLE=8,
            T_INT=9, WRITE=10, L_PAREN=11, R_PAREN=12, L_BRACKET=13, R_BRACKET=14,
            L_BRACE=15, R_BRACE=16, NO_LESS=17, NO_MORE=18, EQUAL=19, UN_EQUAL=20,
            BIGGER_THAN=21, LESS_THAN=22, LOG_NOT=23, LOG_OR=24, LOG_AND=25, PLUS=26,
            MINUS=27, MUL=28, DIV=29, MOD=30, NOT=31, SEMI=32, COMMA=33, ASSIGN=34,
            ID=35, INT=36, DOUBLE=37, ESCAPE=38, WS=39, NEW_LINE=40, B_COMMENT=41,
            L_COMMENT=42, NON_TERMINAL=0;

    public NodeType type;
    public Node parent;
    public LinkedList<Node> child = new LinkedList<Node>();
    public int tokenType;
    public int level;
    public String text;
    public visitFlag flag;

    public Node(NodeType t, Node p, Node c, int to, int l){
        type = t;
        text = t.toString();
        if(p!=null) parent = p;
        if (c!=null) child.add(c);
        tokenType = to;
        level = l;
        flag = visitFlag.unvisited;
    }

    public Node(NodeType t, String s, Node p, Node c, int to, int l){
        type = t;
        text = s;
        if(p!=null) parent = p;
        if (c!=null) child.add(c);
        tokenType = to;
        level = l;
        flag = visitFlag.unvisited;
    }


    public Node(){
        type = NodeType.terminal;
        tokenType = 0;
    }

}
