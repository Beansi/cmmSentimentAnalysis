import java.io.FileInputStream;
import java.util.*;
import java.io.IOException;
import java.io.InputStream;
import org.antlr.v4.runtime.*;

public class   Main {

    public static void main(String[] args) throws IOException{
	// write your code here
        InputStream is = args.length > 0 ? new FileInputStream(args[0]) : System.in;

        ANTLRInputStream input = new ANTLRInputStream(is);
        CmmLex lexer = new CmmLex(input);
        Parser parser = new Parser(lexer.getAllTokens());
        parser.prog();
        parser.visit();

    }
}
