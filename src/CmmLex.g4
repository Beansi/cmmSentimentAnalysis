lexer grammar CmmLex;

FOR : 'for';
IF : 'if';
ELSE : 'else';
ELSE_IF : 'else if';
WHILE :'while';
BREAK : 'break';
Continue : 'continue';

T_DOUBLE : 'double';
T_INT : 'int';
WRITE : 'write';

L_PAREN : '(';
R_PAREN : ')';
L_BRACKET : '[';
R_BRACKET : ']';
L_BRACE : '{';
R_BRACE : '}';

NO_LESS: '>=';
NO_MORE: '<=';
EQUAL: '==';
UN_EQUAL:'!=';
BIGGER_THAN: '>';
LESS_THAN: '<';
LOG_NOT:'!';

LOG_OR:'||';
LOG_AND: '&&';
PLUS : '+';
MINUS : '-';
MUL : '*';
DIV : '/';
MOD : '%';
NOT : '!';
SEMI : ';';
COMMA : ',';
ASSIGN : '=';

//ESC:('\"'|'\\' [btnr"\\]) ;

ID : ID_LETTER (ID_LETTER | DIGIT)* ; // From C language
fragment ID_LETTER : [a-z]|[A-Z]|'_' ;
fragment DIGIT : [0-9] ;
INT: DIGIT+;
DOUBLE : DIGIT+ '.' DIGIT* ;

ESCAPE:  '\\' ['"?abfnrtv\\];
WS :   [ \t\r\n]+  -> skip;
NEW_LINE :   (   '\r' '\n'? |   '\n' ) -> skip;
B_COMMENT :   '/*' .*? '*/' -> skip;
L_COMMENT :  '//' .*? '\r'? '\n' -> skip ;








